var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    username: {type: String, required: true, unique: true},
    pass: {type: String, required: true},
    mail: {type: String},
    fname: String,
    lname: String,
    admin: Boolean,
},
{
    timestamps: true
});

var User = mongoose.model('User',userSchema);

module.exports = User;