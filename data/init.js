var mongoose = require('mongoose');
var crypto = require('crypto');

var secret = 'codepolitan';
var password = crypto.createHmac('sha256', secret)
    .digest('hex');

console.log("Password: " + password);

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/eksprescoba');

var User = require('../models/user');

User.find({username:'superadmin'}, function (err, user){
    if (user.length == 0)
    {
        var admin = new User({
            username: 'superadmin',
            mail: 'admin@example.com',
            pass: password,
            fname: 'super',
            lname: 'admin',
            admin: true,
        });

        admin.save(function(err) {
            if (err) throw err;

            console.log('Admin is created!');
        });
    }
});

User.find({username:'supermember'}, function (err, user){
    if (user.length == 0)
    {
        var member = new User({
            username: 'supermember',
            mail: 'member@example.com',
            pass: password,
            fname: 'super',
            lname: 'member',
            admin: false,
        });

        member.save(function(err) {
            if (err) throw err;

            console.log('Member is created!');
        });
    }
});
