var express = require('express');
var router = express.Router();
var crypto = require('crypto');
var User = require('../models/user');
var Auth_mid = require('../middlewares/auth');

var session_store;

/* GET users listing. */
router.get('/', Auth_mid.check_login,Auth_mid.is_admin, function(req, res, next) {
  session_store = req.session;

  User.find({}, function (err, user) {
      res.render('users/index', {session_store:session_store, users: user});
  }).select('username mail fname lname admin createdAt updatedAt');
});

//Create User
router.post('/add', Auth_mid.check_login, Auth_mid.is_admin, function(req,res,next){
  session_store = req.session;
  req.assert('username','Username wajib di isi!').isAlpha().withMessage('Username harus terdiri dari angka dan huruf').notEmpty();
  req.assert('mail','E-mail tidak valid!').notEmpty().withMessage('Email wajib diisi!').isEmail();
  req.assert('fname','Nama depan harus terdiri dari angka dan huruf!').isAlpha().withMessage('Nama depan wajib di isi!').notEmpty();

  var galat = req.validationErrors();
  console.log(galat);

  if(!galat){
    v_username = req.sanitize('username').escape().trim();
    v_email = req.sanitize('mail').escape().trim();
    v_fname = req.sanitize('fname').escape().trim();
    v_lname = req.sanitize('lname').escape().trim();
    v_akses = req.sanitize('akses').escape().trim();

    var password = crypto.createHmac('sha256',req.param('pwd'))
                  .digest('hex');

    User.find({username:req.param('username')},function(err, user){
      if(user.length == 0){
        // buat user
          var pengguna = new User({
              username: v_username,
              mail: v_email,
              pass: password,
              fname: v_fname,
              lname: v_lname,
              admin: v_akses,
          });
          pengguna.save();
          res.redirect('/users');
      }
    });
  }

});

//Update User
router.put('/edit/(:id)', Auth_mid.check_login, Auth_mid.is_admin, function(req,res, next){
    session_store = req.session;
    req.assert('username','Nama wajib di isi!').isAlpha().withMessage('Username harus terdiri dari angka dan huruf').notEmpty();
    req.assert('mail', 'E-mail tidak valid').isAlpha().notEmpty().withMessage('E-mail diperlukan').isEmail();
    req.assert('fname', 'Nama depan harus terdiri dari angka dan huruf').isAlpha();
    req.assert('lname', 'Nama belakang harus terdiri dari angka dan huruf').isAlpha();

    var error = req.validationErrors();
    console.log(error);

    if(!error){
        v_username = req.sanitize('username').escape().trim();
        v_email = req.sanitize('mail').escape().trim();
        v_fname = req.sanitize('fname').escape().trim();
        v_lname = req.sanitize('lname').escape().trim();
        v_akses = req.sanitize('hakakses').escape().trim();

        User.findById(req.params.id, function(err, user){
            user.username = req.param('username');
            user.mail = req.param('mail');
            user.fname = req.param('fname');
            user.lname = req.param('lname');
            user.admin =  v_akses;

            user.save();
            res.redirect('/users');
        });
    }
});

//Delete User

router.delete('/delete/(:id)', Auth_mid.check_login, Auth_mid.is_admin, function(req,res,next){
    User.findById(req.params.id, function(err, user){
        user.remove();
        res.redirect('/users');
    });
});

module.exports = router;
